<?php
 
/**
 * Implements a term flag.
 */
class flag_message extends flag_flag {
 function options() {
    $options = parent::options();
    return $options;
  }

  function options_form(&$form) {
    parent::options_form($form);

    $options = array();

    $form['access']['types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('There are no access options.'),
    );
  }

  function _load_content($content_id) {
    return message_instance_load($content_id);
  }

  function get_content_id($message) {
    return $message->iid;
  }
  
  function applies_to_content_object($node) {
      return TRUE;
  }
  
  function get_labels_token_types() {
    return array('message');
  }

  function replace_tokens($label, $contexts, $content_id) {
    if ($content_id && ($message = $this->fetch_content($content_id))) {
      $contexts['message'] = $message;
    }
    return parent::replace_tokens($label, $contexts, $content_id);
  }

  function get_flag_action($content_id) {
    $flag_action = parent::get_flag_action($content_id);
    $term = $this->fetch_content($content_id);
    $flag_action->content_title = $message->name;
    $flag_action->content_url = _flag_url('messages/' . $message->iid);
    return $flag_action;
  }

  function get_relevant_action_objects($content_id) {
    return array(
      'message' => $this->fetch_content($content_id),
    );
  }

  function rules_get_event_arguments_definition() {
    return array(
      'message' => array(
        'type' => 'message',
        'label' => t('flagged message'),
        'handler' => 'flag_rules_get_event_argument',
      ),
    );
  }

  function rules_get_element_argument_definition() {
    return array('type' => 'message', 'label' => t('Flagged Message'));
  }
  
  function get_views_info() {
    return array(
      'views table' => 'message_instance',
      'join field' => 'iid',
      'title field' => 'name',
      'title' => t('Message flag'),
      'help' => t('Limit results to only those terms flagged by a certain flag; Or display information about the flag set on a term.'),
      'counter title' => t('Message flag counter'),
      'counter help' => t('Include this to gain access to the flag counter field.'),
    );
  }

 
}